# README.md

- install nodejs from [documentation](https://github.com/nodesource/distributions#using-ubuntu-3)

- clone this repo and run
    ```
    npm install
    node app.js
    ```
- open browser to `localhost:8080`
